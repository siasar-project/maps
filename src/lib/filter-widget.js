import * as GeoDashboard from 'geodashboard';

import template from '../templates/filter-widget.hbs';
import optionsTemplate from '../templates/adm-options.hbs';

import '../styles/filter-widget.scss';

class FilterWidget extends GeoDashboard.Widget {
  constructor(config) {
    super(config);
    this.template = template;
    this.className = 'filter-widget';

    this.value = {};
  }

  render() {
    super.render();

    this.options = {
      adm0: {
        BO: this.manager.dashboard.i18n.t('countries.bo'),
        BR: this.manager.dashboard.i18n.t('countries.br'),
        CO: this.manager.dashboard.i18n.t('countries.co'),
        CR: this.manager.dashboard.i18n.t('countries.cr'),
        DO: this.manager.dashboard.i18n.t('countries.do'),
        HN: this.manager.dashboard.i18n.t('countries.hn'),
        MX: this.manager.dashboard.i18n.t('countries.mx'),
        NI: this.manager.dashboard.i18n.t('countries.ni'),
        PA: this.manager.dashboard.i18n.t('countries.pa'),
        PY: this.manager.dashboard.i18n.t('countries.py'),
        PE: this.manager.dashboard.i18n.t('countries.pe'),
        KG: this.manager.dashboard.i18n.t('countries.kg'),
      },
    };

    this.content.innerHTML = this.template();

    this.adm0 = document.getElementById('adm0');
    this.adm1 = document.getElementById('adm1');
    this.adm2 = document.getElementById('adm2');

    this.fillOptions(this.adm0, this.options.adm0);

    this.manager.filters.forEach((filter) => {
      if (filter.property === 'country') {
        this.adm0.style.display = 'none';
        this.getAdmValues(this.adm1, `0/${filter.value}`);
      }
    });

    Array.prototype.forEach.call(document.querySelectorAll(`#${this.id} select`), (adm) => {
      adm.addEventListener('change', this.filter.bind(this));
    });
  }

  refresh() {
    // Just to override super function
  }

  filter(event) {
    const { target } = event;
    const filters = [];

    if (target === this.adm0) {
      this.value.adm0 = this.adm0.value || null;
      this.value.adm1 = null;
      this.value.adm2 = null;
      this.adm1.innerHTML = '';
      this.adm2.innerHTML = '';
      this.dashboard.mapZoom = 3;

      if (this.value.adm0) {
        this.getAdmValues(this.adm1, `0/${this.value.adm0}`);
      }
    } else if (target === this.adm1) {
      this.value.adm0 = this.adm0.value;
      this.value.adm1 = this.adm1.value || null;
      this.value.adm2 = null;
      this.adm2.innerHTML = '';
      this.dashboard.mapZoom = 5;

      if (this.value.adm1) {
        this.getAdmValues(this.adm2, `${this.value.adm1}/all`);
      }
    } else if (target === this.adm2) {
      this.value.adm0 = this.adm0.value;
      this.value.adm1 = this.adm1.value;
      this.value.adm2 = this.adm2.value || null;
      this.dashboard.mapZoom = 8;
    }

    Object.keys(this.value).forEach((adm) => {
      if (this.value[adm]) {
        filters.push(new GeoDashboard.Filter({
          property: this[adm].dataset.property,
          value: adm === 'adm0' ? this.value[adm] : this.options[adm][this.value[adm]],
        }));
      }
    });

    this.dashboard.mapManager.once('loaded', this.dashboard.autoZoom.bind(this.dashboard));
    this.dashboard.resetFilters(filters);
  }

  fillOptions(target, options) {
    target.innerHTML = optionsTemplate({
      options,
    });
  }

  getAdmValues(target, filter) {
    fetch(`http://staging.siasar.org/rest/location_hierarchy/children/${filter}`, {
      method: 'GET',
    }).then(response => response.json()).then((data) => {
      this.options[target.id] = {};
      data.forEach((adm) => {
        this.options[target.id][adm.tid] = adm.name;
      });
      this.fillOptions(target, this.options[target.id]);
    });
  }
}

export default FilterWidget;
