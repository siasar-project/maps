# [SIASAR](http://siasar.org) Dashboard

Aplicación web creada con GeoDashboard y GeoServer para mostrar datos del proyecto [SIASAR](http://siasar.org).

GeoDashboard usa servicios WMS, WFS y WPS para mostrar datos en mapas y widgets.

[![build status](https://gitlab.com/Admin_Siasar/SIASAR-Dashboard/badges/master/build.svg)](https://gitlab.com/Admin_Siasar/SIASAR-Dashboard/commits/master)

## Cómo correrlo localmente

1. Clonar repositorio  
`git clone https://gitlab.com/Admin_Siasar/SIASAR-Dashboard.git`  

1. Instalar dependencias  
`npm install`  

1. Correr server local de desarrollo  
`npm run serve`  

1. O compilar versión para implementación  
`npm run build`
